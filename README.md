# CB_Plus_Exercise_1

First Exercise of the technical test, divided into 2 parts.

## Part A
### Instruction to run the command line tool

A small shell script is available to lunch the python program.
```console
shell$ ./run_script.sh
```
You can also launch the program by directly calling the python main script:
```console
shell$ python ./src/run_prompt.py
```
Once in the command line tool, you will be able to use these commands:
- *help*
- *read_shop_db*
- *read_expiry_app_db*
- *analyze*
- *save*
- *quit*

### Datas

In order to work as expected, it is recommended to follow this architecture:
```
data/
├── references initialized in shop.csv
└── retailer extract vLB.xls
```
I met some difficulties to properly read the `references initialized in shop.csv` file. For this reason it has been modified in order to be loaded properly.
Since `retailer extract vLB.xls` is heavy and does not need any modification, I let you paste it into the `data/` folder.

### Commands

Here is a small description of each commands.
- `help` and `help <command>` will list all commands and display the help for a specified command.
- `read_shop_db <optional_path>` will open and load the file of the database from the shop internal system. If no path is given, it will open the file 'data/retailer extract vLB.xls'.
- `read_expiry_app_db <optional_path>` will open and load the file of the database from the ExpiryApp. If no path is given, it will open the file 'data/references initialized in shop.csv'.
- `analyze` will run the algorithm to detect and display the missing and meaningful references.
- `save <optional_path>` will save the output of the analysis in a CSV file (by default ./out.csv)
- `quit` will close the command line. CTRL + D works too.

### Explanatin of the analysis

First of all, the first step to start the analysis is to find a common identifier for a same product between the two databases. The barcode is the ideal id for this since it is unique for each product.

Then, before analyzing, the data need to be cleaned. In the retailer's database, all references without a stock are dismissed since they are not physically present in the shop. Moreover, only the useful columns are kept in order to lighten our dataframes.

A new column is created to identify the type of a product. This new column is composed of the Family and sub-Family identifiers. This column will allow us to determine if recommending an unreferenced product is meaningful.

Now, the two dataframes are joined together.

the dataframe from ExpiryApp:

|aisle|EAN|
|---|---|
|Pain de mie|3029330071136|
|Pain de mie|3029330071105|

the dataframe frome the retailer system:

|Code Famille-merged|EAN|Stock en quantité|Article Libellé Long|
|---|---|---|---|
|2572-005|3215061072751|12|Gauffre exemple 1|
|2570-000|3029330071105|5|Pain de mie Bio|
|2570-000|3029330071136|17|Pain de mie céréales|
|2570-000|3029330071150|4|Pain de mie sans croute|

the dataframe created from the JOIN:

|Code Famille-merged|EAN|Stock en quantité|Article Libellé Long|aisle|
|---|---|---|---|---|
|2572-005|3215061072751|12|Gauffre exemple 1|Null|
|2570-000|3029330071105|5|Pain de mie Bio|Pain de mie|
|2570-000|3029330071136|17|Pain de mie céréales|Pain de mie|
|2570-000|3029330071150|4|Pain de mie sans croute|Null|

From this final dataframe we can now list all family not meaningful for the App. If the field `aisle` is `Null`, it means this reference does not exist in the ExpiryApp database. Thanks to this new dataframes we can also determine what are the meaningful family for the retailer. A family is useful if at least one of the references of this family have its `aisle` field defined. In other word, it means the retailer have entered a product from this family in the Expiry App.
Since we know all the useful family, we can drop all the products the retailer does not want to control.
The last step is to list all the remaining rows where the `aisle` field is null.

To suggest an aisle to the retailer, we take the `aisle` of the products with the same family code. But a family of product can be sold in various aisles, for this reason we only keep the most common one.

### Technical decision and areas of improvment

Pandas is a powerful tool to manipulate data. It is easy to use, quiet intuitive and fast enough for this case. Nonetheless it is probably not the fastest tool to analyze data, especially if we are dealing with a heavier dataset. If performances are important and seconds or milliseconds are critical, SQL may be a good alternative, but it also requires more time to deploy and to setup the database.
Moreover, in order to be sure to handle special and corner cases, an important plus would be unit tests.
An other area of improvment is the suggestion of the aisle for unregistered product. Is it possible to be more accurate by using the field `Code Unité de Besoin`. Moreover some existing aisles are redundant (e.g '37.2 Patisserie & viennoiserie' and 'viennoiserie'). Using the most common one when a product belong to both of them may not be the better solution in all scenarios.

## Part B

First of all, we can identify the features useful to the appropriate discount.

- The expiry date is a key element. The closer we get to it, the ggreater the discount will be.
- The quantity of this product close the expiry date. It can also be interesting to suggest to sell this product by batch.
- If this product is popular on this period of the year.
- If this product is popular at this time of the day.

We can easily get the two first features from the database given by the retailer. Unfortunetly it is harder to determine the last two features since we do not have access to this information. Ideally we could find a way to create this information, but it requires to do it manually by annotating a suffisant amount of reference.

A good solution for this tool can be to use random forests. Random forests use several decision trees for classification or regression.
This solution would require generating data to train the algorithm. In order to make the random forest efficient, we need to annotate enough product, in different condition (i.e different time of the day, different seasons, different days before expiry day etc. for the same reference).
Considering we can have access to these kind of information and store them in a database, the following steps would be to test several configuations for the random forest. We can benchmark some configurations with a notebook. The model can be trained using Scikit-learn.

A other part of this project is the tool used directly by the user. The interface need to be created and plugged to the already existing ExpiryApp. Given the product identifier, the application will determine the number of days before the perishing date, the remaining stock perishing soon, the popolarity of the product at this time of the day and the year. Then, the trained model will be able to predict the most efficient discount.

The success of this method rely on the accuracy of the annotation of the training data. This annotation must be done with the knowledge of professional from the retailing sector. They know perfectly the habits of the customers and what is the optimal discount for a product.

In the solution exposed above, the easiest but most time consumming part is probably the data annotation. To be efficient the dataset shoul be bigger than 2000 exemples.
A first prototype of the tool can be developed in less than 5 days, considering the research for the optimal parameters for the random forest, and the development of the interface in the already existing application.