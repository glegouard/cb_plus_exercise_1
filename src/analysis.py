import pandas as pd

def analysis(df_expiry_app, df_shop_db, verbose=True):
    df_join = df_shop_db.join(df_expiry_app.set_index('EAN'), on='EAN')

    df_registred_ref = df_join.loc[df_join['aisle'].notnull()]
    list_registred_family = df_registred_ref['Code Famille-merged'].unique()

    df_res = df_join.loc[df_join['Code Famille-merged'].isin(list_registred_family) & df_join['aisle'].isnull()]
    recommended_aisles = []

    for _, row in df_res.iterrows():
        df_similar_products = df_registred_ref[df_registred_ref['Code Famille-merged'] == row['Code Famille-merged']]
        most_common_aisle = df_similar_products.groupby('aisle')['aisle'].count().sort_values(ascending=False)
        recommended_aisles.append(most_common_aisle.idxmax())
        if verbose:
            print('Reference: ', row['EAN'], ' - ', row['Article Libellé Long'], 'is not tracked, similar products are in aisle: ', most_common_aisle.idxmax())

    if verbose:
        print('Total: ', len(df_res.index), ' meaningful products can be registered.')
    
    df_res = df_res.copy()
    df_res.loc[:, 'recommended aisles'] = recommended_aisles
    columns = ['EAN', 'Article Libellé Long', 'recommended aisles']
    df_return = df_res[columns]
    return df_return