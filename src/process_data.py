import pandas as pd

def process_data_expiry_app(dataframe):
    columns = ['aisle', 'reference_id']
    dataframe = dataframe[columns]
    dataframe = dataframe.rename(columns={'reference_id':'EAN'})#, inplace=True)
    dataframe = dataframe.astype({'EAN': 'int64'})
    return dataframe

def process_data_shop(dataframe):
    columns = ['Code Famille ', 'Code Sous-Famille ', 'EAN', 'Stock en quantité', 'Article Libellé Long']
    dataframe = dataframe[columns]
    dataframe = dataframe.loc[dataframe['Stock en quantité'] > 0]
    dataframe['Code Famille-merged'] = dataframe['Code Famille '].map(str) + "-" + dataframe['Code Sous-Famille '].map(str)
    dataframe = dataframe.drop(['Code Famille ', 'Code Sous-Famille '], axis=1)
    return dataframe