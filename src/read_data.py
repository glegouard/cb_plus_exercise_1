import pandas as pd

def read_data(path):
    print('Reading file: ', path, ' ')
    datas = pd.DataFrame()
    if path.endswith('.csv'):
        datas = pd.read_csv(path)
        print('Done')

    elif path.endswith('.xls'):
        datas = pd.read_excel(path)
        print('Done')

    else:
        print('Error, please submit a CSV or XLS file')
        return

    if datas.empty:
        print('The loading of the data failed')

    return datas
