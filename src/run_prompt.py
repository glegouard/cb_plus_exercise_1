import read_data as rd
import process_data as prdt
import analysis as an
import cmd
import pandas as pd

class Cli_expiryApp(cmd.Cmd):
    prompt = '$ '
    intro = ' Command line tool for suggesting unregistred products'
    df_expiry_app = pd.DataFrame()
    df_shop_db = pd.DataFrame()
    df_analyzed_db = pd.DataFrame()

    def do_read_expiry_app_db(self, path):
        """
read_expiry_app_db <path>: Load and parse database file given by <path>
if no path is provided, `data/references initialized in shop.csv` is used as default
        """
        if path == '':
            path ='data/references initialized in shop.csv'
        df_expiry_app = rd.read_data(path)
        self.df_expiry_app = prdt.process_data_expiry_app(df_expiry_app)

    def do_read_shop_db(self, path):
        """
read_shop_db <path>: Load and parse database file given by <path>
if no path is provided, `data/retailer extract vLB.xls` is used as default
        """
        if path == '':
            path = 'data/retailer extract vLB.xls'
        df_shop_db = rd.read_data(path)
        self.df_shop_db = prdt.process_data_shop(df_shop_db)

    def do_analyze(self, line):
        """
analyze: launch the algorithm to determine untracked and meaningful reference
/!\\ databases must be loaded before using this command
        """
        if self.df_expiry_app.empty or self.df_shop_db.empty:
            print("Please load database from the shop and the ExpriryApp before with `do_read_expiry_app_db` and `do_read_shop_db` commands")
        else:
            self.df_analyzed_db = an.analysis(self.df_expiry_app, self.df_shop_db)

    def do_save(self, path):
        """
save <path>: save the results in a file
/!\\ databases must be loaded before using this command
        """
        if self.df_analyzed_db.empty:
            self.df_analyzed_db = an.analysis(self.df_expiry_app, self.df_shop_db, False)
        if path == '':
            path = 'missing_references.csv'
        self.df_analyzed_db.to_csv(path, index=False)


    def do_EOF(self, line):
        """
Quit using ctrl + D
        """
        return True

    def do_quit(self, line):
        """
Quit the command line tool
        """
        return True

if __name__ == "__main__":
    Cli_expiryApp().cmdloop()